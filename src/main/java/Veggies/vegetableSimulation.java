/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Veggies;

/**
 *
 * @author grant
 */
import java.util.ArrayList;
public class vegetableSimulation {
    
    public static void main (String[]args){
           ArrayList <Veggies> veg = new ArrayList<>();
        vegetableFactory factory = vegetableFactory.makeFactory();
     
        veg.add(factory.grabVeggie("carrot", Boolean.TRUE));
        veg.add(factory.grabVeggie("carrot", Boolean.FALSE));
        veg.add(factory.grabVeggie("beet", Boolean.TRUE));
        veg.add(factory.grabVeggie("beet", Boolean.FALSE));
        
         for (int x=0;x<4;x++){ 
             veg.get(x).isRipe();
         }
    }
    
}
