/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package PayRoll;


public class PayrollSimulation {
    
    public static void main (String [] args){
        
        EmployeeFactory factory = EmployeeFactory.makeFactory();
       
        Employee bob = factory.makeEmployee("Employee","bob", 15, 35);
        Employee rob = factory.makeEmployee("manager","rob", 25, 40);
        bob.calculatePay();
        rob.calculatePay();
    }
}
