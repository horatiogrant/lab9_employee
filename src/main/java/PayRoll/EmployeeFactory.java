/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package PayRoll;

import java.util.Scanner;


public class EmployeeFactory {
Scanner kb = new Scanner(System.in);
public static EmployeeFactory factory;

private EmployeeFactory(){}

public static EmployeeFactory makeFactory(){
    
    if(factory==null){
        factory = new EmployeeFactory();
    }
    return factory;
}

public Employee makeEmployee(String position, String name, double wages, double hours){
    
    if (position.equalsIgnoreCase("manager")){
        System.out.println("Enter Managers Bonus");
        return new Manager(name, wages, hours, kb.nextDouble());
    }else if(position.equalsIgnoreCase("employee")){
        return new Employee(name, wages, hours);
    }
    
    
    return null;
}


}
