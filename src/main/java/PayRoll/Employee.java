/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package PayRoll;

public class Employee {
    
    private String name;
    private double wage;
    private double hours;
    
    public Employee (String name, double wage, double hours){
        this.name=name;
        this.wage=wage;
        this.hours=hours;
    }
    public void calculatePay(){
        System.out.println("Employee "+getName()+"\n pay = "+(getWage()*getHours()));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWage() {
        return wage;
    }

    public void setWage(double wage) {
        this.wage = wage;
    }

    public double getHours() {
        return hours;
    }

    public void setHours(double hours) {
        this.hours = hours;
    }
    
    
}
